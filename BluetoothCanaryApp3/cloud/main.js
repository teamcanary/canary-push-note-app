
// Use Parse.Cloud.define to define as many cloud functions as you want.
// For example:
Parse.Cloud.define("hello", function(request, response) {
  response.success("Hello world!");
  console.log("test");
});

function get_guest(){
	var Guests = Parse.Object.extend("Guests");
	var query = new Parse.Query(Guests);
	query.get("6MKWuyZSZI", {
	  success: function(Guests) {
	    // The object was retrieved successfully.
	    var guestName = Guests.get("guestName");
		var present = Guests.get("present");
		if (present){
			// send push notificaton to the owner of the guest
				Parse.Push.send({
				  channels: [ "Maria" ],
				  data: {
				    alert: "Hi, your friend Jeff is here. "
				  }
				}, {
				  success: function() {
				    // Push was successful
				    console.log("successful push")
				  },
				  error: function(error) {
				    // Handle error
				  }
	  			});
	  	}
	  	else{
				Parse.Push.send({
				  channels: [ "Maria" ],
				  data: {
				    alert: "Hi, your friend Jeff has left. "
				  }
				}, {
				  success: function() {
				    // Push was successful
				    console.log("successful push")
				  },
				  error: function(error) {
				    // Handle error
				  }
				});
		}
	  },
	  error: function(object, error) {
	    // The object was not retrieved successfully.
	    // error is a Parse.Error with an error code and message.
	  }
	});
}

function test(){
	  console.log("entered test");
	Parse.Push.send({
	  channels: [ "global" ],
	  data: {
	    alert: "Hi, your friend Jeff is here. "
	  }
	}, {
	  success: function() {
	    // Push was successful
	    console.log("successful push")
	  },
	  error: function(error) {
	    // Handle error
	  }
	});
}
// This will send a push notification after guest has been marked as arried by ios app
Parse.Cloud.afterSave("Guests", function(request) {
  get_guest();
  console.log("Entered afterSave");
  // handleRequest(request);
  // if (error != null) {
  //   console.log(error);
  //   return;
  // }
});
function handleRequest(request) {
  var arrived = request.object.arrived;
  if (arrived == true){
	var query = new Parse.Query(Parse.Installation);
	Parse.Push.send({
	  where: query, // Set our Installation query
	  data: {
	    alert: "Broadcast to everyone"
	  }
	}, {
	  success: function() {
	    // Push was successful
	  },
	  error: function(error) {
	    // Handle error
	  }
	});
    }
 }