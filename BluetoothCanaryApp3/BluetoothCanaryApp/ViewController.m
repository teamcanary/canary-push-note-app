//
//  ViewController.m
//  BluetoothCanaryApp
//
//  Created by Jeff Ponnor and Haiwei Su on 10/23/15.
//  Copyright © 2015 Jeff Ponnor. All rights reserved.
//

#import "ViewController.h"
#import "CBManagerDelegate.h"
#import <Parse/Parse.h>


@interface ViewController ()
@property CBManagerDelegate *CBC;
@property (weak, nonatomic) IBOutlet UILabel *signalLevelLabel;
@property (weak, nonatomic) IBOutlet UIProgressView *signalSlider;
@property (weak, nonatomic) IBOutlet UILabel *statusLabel;
@property (weak, nonatomic) IBOutlet UIImageView *canaryimage;
@property (weak, nonatomic) IBOutlet UIImageView *canaryarmedimage;
@property (nonatomic) BOOL armed;
@property (nonatomic) BOOL send;


@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    self.CBC = [CBManagerDelegate alloc];
    
    // get current date/time
    NSDate *today = [NSDate date];
    NSLog(@"today's date");
    NSLog(@"%@",today);
    
    PFQuery *query = [PFQuery queryWithClassName:@"Guests"];
    [query getObjectInBackgroundWithId:@"gAEyJYZqkZ" block:^(PFObject *guest, NSError *error) {
        // Do something with the returned PFObject in the gameScore variable.
        NSDate *startTime = [guest objectForKey:@"startTime"];
        NSDate *endTime = [guest objectForKey:@"endTime"];
        NSLog(@"%@", startTime);
        NSLog(@"this is end time:");
        NSLog(@"%@", endTime);
        NSLog(@"%@", today);
        //checkTimeRange(startTime, endTime, today);
        
    }];

    
    if(self.CBC){
        self.CBC.cBCM = [[CBCentralManager alloc] initWithDelegate:self.CBC queue:nil];
        
        
    }
    
    self.armed = TRUE;
    self.send = FALSE;
    //set NSUserStart
    //NSInteger startSignal = 0;
    //[[NSUserDefaults standardUserDefaults] setInteger:startSignal forKey:@"Start"];

    
    
    
}



- (void) beaconSearch {
    
    
    [self.CBC.cBCM scanForPeripheralsWithServices:nil options:nil];
    
    //display signal level
    NSNumber *signalLevel = [[NSUserDefaults standardUserDefaults] objectForKey:@"Signal"];
    self.signalLevelLabel.text = [signalLevel stringValue];
    
    //If within range...
    if([signalLevel doubleValue]>-70){
        
        //Disarm the Canary
        if (self.armed == TRUE){
            self.armed = FALSE;
            
        
            if (self.send == TRUE){
        PFQuery *query = [PFQuery queryWithClassName:@"Guests"];
        // Retrieve the object by id
        [query getObjectInBackgroundWithId:@"6MKWuyZSZI"
                                     block:^(PFObject *guest, NSError *error) {
                                         // Now let's update it with some new data. In this case, only cheatMode and score
                                         // will get sent to the cloud. playerName hasn't changed.
                                         guest[@"arrived"] = @YES;
                                         [guest saveInBackground];
                                     }];
                
            }
        }
        
        self.canaryarmedimage.alpha = 0.0f;
        UIColor *color = [UIColor greenColor];
        self.statusLabel.text = @"Disarmed";
        self.statusLabel.textColor = color;
        
        PFQuery *query = [PFQuery queryWithClassName:@"Guests"];
        
        // Retrieve the object by id 6MKWuyZSZI
        [query getObjectInBackgroundWithId:@"6MKWuyZSZI"
                                     block:^(PFObject *guest, NSError *error) {
                                         // Now let's update it with some new data. In this case, only cheatMode and score
                                         // will get sent to the cloud. playerName hasn't changed.
                                         guest[@"arrived"] = @YES;
                                         [guest saveInBackground];
                                     }];
//        PFQuery *query = [PFQuery queryWithClassName:@"Guests"];
//        [query getObjectInBackgroundWithId:@"gAEyJYZqkZ" block:^(PFObject *guest, NSError *error) {
//            // Do something with the returned PFObject in the gameScore variable.
//            NSLog(@"%@", guest);
//        }];
    }
    //otherwise return to armed state
    
    
    
    
    
    //Else if not in range return to Armed state
    else{
        
        if (self.armed == FALSE){
            self.armed = TRUE;
            
            if (self.send == TRUE){
            PFQuery *query = [PFQuery queryWithClassName:@"Guests"];
            // Retrieve the object by id
            [query getObjectInBackgroundWithId:@"IIhvcMbqjZ"
                                         block:^(PFObject *guest, NSError *error) {
                                             // Now let's update it with some new data. In this case, only cheatMode and score
                                             // will get sent to the cloud. playerName hasn't changed.
                                             guest[@"left"] = @YES;
                                             [guest saveInBackground];
                                         }];
            }
        
        }
        //Arm Canary
        self.canaryarmedimage.alpha = 1.0;
        UIColor *color = [UIColor redColor];
        self.statusLabel.text = @"Armed";
        self.statusLabel.textColor = color;
    }
}


- (IBAction)generateList:(id)sender {
    
    
    //if(    [[NSUserDefaults standardUserDefaults] integerForKey:@"Start"] == 0){
    //[self.CBC.cBCM scanForPeripheralsWithServices:nil options:nil];
    //}
    //else{
    
    //display signal level
    //    NSNumber *signalLevel = [[NSUserDefaults standardUserDefaults] objectForKey:@"Signal"];
    //    self.signalLevelLabel.text = [signalLevel stringValue];
    //    if([signalLevel doubleValue]>-70){
    //        self.statusLabel.text = @"Canary Disarmed";
    //    }
    
    //    else{
    //        self.statusLabel.text = @"Canary Armed";
    
    //    }
    
    //restart search
    //    NSInteger startSignal = 0;
    //    [[NSUserDefaults standardUserDefaults] setInteger:startSignal forKey:@"Start"];
    
    //}
    
    //
    
    //Runs when the Canary is armed
    //Every 0.1 seconds search for the list of the beacons
    self.canaryarmedimage.alpha = 1.0;
    [NSTimer scheduledTimerWithTimeInterval:0.9f target:self selector:@selector(beaconSearch) userInfo:nil repeats:YES];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end


